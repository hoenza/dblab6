"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubTaskModule = void 0;
const common_1 = require("@nestjs/common");
const sub_task_controller_1 = require("./sub-task.controller");
const sub_task_service_1 = require("./sub-task.service");
let SubTaskModule = class SubTaskModule {
};
SubTaskModule = __decorate([
    common_1.Module({
        providers: [sub_task_service_1.SubTaskService],
        controllers: [sub_task_controller_1.default]
    })
], SubTaskModule);
exports.SubTaskModule = SubTaskModule;
//# sourceMappingURL=sub-task.module.js.map
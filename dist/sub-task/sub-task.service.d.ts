import SubTaskEntity from 'src/db/entity/subTask.entity';
import CreateSubTaskDto from 'src/todo/dto/create-subTask.dto';
export declare class SubTaskService {
    insert(subTaskDetails: CreateSubTaskDto): Promise<SubTaskEntity>;
    getAllSubTasks(): Promise<SubTaskEntity[]>;
    delete(subTaskID: number): Promise<SubTaskEntity>;
    update(subTaskID: number, subTaskDetails: CreateSubTaskDto): Promise<SubTaskEntity>;
}

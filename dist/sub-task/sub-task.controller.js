"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const dist_1 = require("@nestjs/swagger/dist");
const subTask_entity_1 = require("../db/entity/subTask.entity");
const task_entity_1 = require("../db/entity/task.entity");
const create_subTask_dto_1 = require("../todo/dto/create-subTask.dto");
const sub_task_service_1 = require("./sub-task.service");
const swagger_1 = require("@nestjs/swagger");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
let SubTaskController = class SubTaskController {
    constructor(subTaskService) {
        this.subTaskService = subTaskService;
    }
    insertTask(task) {
        return this.subTaskService.insert(task);
    }
    getAlltasks() {
        return this.subTaskService.getAllSubTasks();
    }
    deletetask(subTaskID) {
        return this.subTaskService.delete(subTaskID);
    }
    updatetask(subTaskID, subTask) {
        return this.subTaskService.update(subTaskID, subTask);
    }
};
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'will handle the creating of new sub-task' }),
    common_1.Post(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_subTask_dto_1.default]),
    __metadata("design:returntype", void 0)
], SubTaskController.prototype, "insertTask", null);
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'returns the list of all the existing sub-tasks in the database' }),
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], SubTaskController.prototype, "getAlltasks", null);
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'deletes subtask with id' }),
    common_1.Delete(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    common_1.Delete(),
    __param(0, common_1.Query('subTaskID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SubTaskController.prototype, "deletetask", null);
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'will handle the updateing of a sub-task' }),
    common_1.Put(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    common_1.Put(),
    __param(0, common_1.Query('subTaskID')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_subTask_dto_1.default]),
    __metadata("design:returntype", Promise)
], SubTaskController.prototype, "updatetask", null);
SubTaskController = __decorate([
    common_1.Controller('subtask'),
    __metadata("design:paramtypes", [sub_task_service_1.SubTaskService])
], SubTaskController);
exports.default = SubTaskController;
//# sourceMappingURL=sub-task.controller.js.map
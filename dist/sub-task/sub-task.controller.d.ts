import SubTaskEntity from "src/db/entity/subTask.entity";
import CreateSubTaskDto from "src/todo/dto/create-subTask.dto";
import { SubTaskService } from "./sub-task.service";
export default class SubTaskController {
    private readonly subTaskService;
    constructor(subTaskService: SubTaskService);
    insertTask(task: CreateSubTaskDto): Promise<SubTaskEntity>;
    getAlltasks(): Promise<SubTaskEntity[]>;
    deletetask(subTaskID: any): Promise<SubTaskEntity>;
    updatetask(subTaskID: any, subTask: CreateSubTaskDto): Promise<SubTaskEntity>;
}

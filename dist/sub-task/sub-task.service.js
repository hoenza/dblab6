"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubTaskService = void 0;
const category_entity_1 = require("../db/entity/category.entity");
const label_entity_1 = require("../db/entity/label.entity");
const subTask_entity_1 = require("../db/entity/subTask.entity");
const task_entity_1 = require("../db/entity/task.entity");
const user_entity_1 = require("../db/entity/user.entity");
const create_subTask_dto_1 = require("../todo/dto/create-subTask.dto");
class SubTaskService {
    async insert(subTaskDetails) {
        const subTask = new subTask_entity_1.default();
        subTask.description = subTaskDetails.description;
        subTask.task = await task_entity_1.default.findOne(subTaskDetails.taskID);
        await subTask.save();
        return subTask;
    }
    async getAllSubTasks() {
        return subTask_entity_1.default.find();
    }
    async delete(subTaskID) {
        const SubTask = await subTask_entity_1.default.findOne(subTaskID);
        return SubTask.remove();
    }
    async update(subTaskID, subTaskDetails) {
        const subTask = await subTask_entity_1.default.findOne(subTaskID);
        subTask.description = subTaskDetails.description;
        subTask.task = await task_entity_1.default.findOne(subTaskDetails.taskID);
        await subTask.save();
        return subTask;
    }
}
exports.SubTaskService = SubTaskService;
//# sourceMappingURL=sub-task.service.js.map
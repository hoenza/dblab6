import { BaseEntity } from 'typeorm';
import UserEntity from './user.entity';
export default class BookEntity extends BaseEntity {
    id: number;
    name: string;
    user: UserEntity;
}

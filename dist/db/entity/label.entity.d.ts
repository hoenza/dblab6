import { BaseEntity } from 'typeorm';
export default class LabelEntity extends BaseEntity {
    id: number;
    name: string;
}

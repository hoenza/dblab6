"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user.entity");
const common_1 = require("@nestjs/common");
const category_entity_1 = require("./category.entity");
const label_entity_1 = require("./label.entity");
const subTask_entity_1 = require("./subTask.entity");
let TaskEntity = class TaskEntity extends typeorm_1.BaseEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], TaskEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ length: 500 }),
    __metadata("design:type", String)
], TaskEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(() => category_entity_1.default, category => category.tasks, {
        eager: true
    }),
    __metadata("design:type", category_entity_1.default)
], TaskEntity.prototype, "category", void 0);
__decorate([
    common_1.Optional(),
    typeorm_1.ManyToMany(() => label_entity_1.default, {
        eager: true
    }),
    typeorm_1.JoinTable(),
    __metadata("design:type", Array)
], TaskEntity.prototype, "labels", void 0);
__decorate([
    common_1.Optional(),
    typeorm_1.Column({ length: 500 }),
    __metadata("design:type", String)
], TaskEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.default, user => user.tasks, {
        onDelete: 'CASCADE'
    }),
    __metadata("design:type", user_entity_1.default)
], TaskEntity.prototype, "user", void 0);
__decorate([
    typeorm_1.OneToMany(() => subTask_entity_1.default, subTask => subTask.task, {
        eager: true
    }),
    __metadata("design:type", Array)
], TaskEntity.prototype, "subTasks", void 0);
TaskEntity = __decorate([
    typeorm_1.Entity()
], TaskEntity);
exports.default = TaskEntity;
//# sourceMappingURL=task.entity.js.map
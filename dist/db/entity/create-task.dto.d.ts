export default class CreateTaskDto {
    readonly name: string;
    readonly description: string;
    readonly userID: number;
    readonly subTasks: string[];
    readonly category: number;
    readonly labels: number[];
}

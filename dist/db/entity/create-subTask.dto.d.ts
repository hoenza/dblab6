export default class CreateSubTaskDto {
    readonly description: string;
    readonly taskID: number;
}

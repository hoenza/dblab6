"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const api_property_decorator_1 = require("@nestjs/swagger/dist/decorators/api-property.decorator");
const class_validator_1 = require("class-validator");
class CreateTaskDto {
}
__decorate([
    api_property_decorator_1.ApiProperty({
        description: 'name of the task',
        maxLength: 500,
        example: 'temp task',
        type: String
    }),
    __metadata("design:type", String)
], CreateTaskDto.prototype, "name", void 0);
__decorate([
    common_1.Optional(),
    api_property_decorator_1.ApiPropertyOptional({
        description: 'the description of the task',
        maxLength: 500,
        example: 'this task is so arduous',
        type: String
    }),
    __metadata("design:type", String)
], CreateTaskDto.prototype, "description", void 0);
__decorate([
    class_validator_1.IsNumber(),
    api_property_decorator_1.ApiProperty({
        description: 'associated user id',
        example: '1',
        type: Number
    }),
    __metadata("design:type", Number)
], CreateTaskDto.prototype, "userID", void 0);
__decorate([
    class_validator_1.IsArray(),
    common_1.Optional(),
    api_property_decorator_1.ApiPropertyOptional({
        description: 'array of the subtasks\' descriptions',
        example: ['subtask1', 'subtask2'],
        type: Array(String)
    }),
    __metadata("design:type", Array)
], CreateTaskDto.prototype, "subTasks", void 0);
__decorate([
    class_validator_1.IsNumber(),
    api_property_decorator_1.ApiProperty({
        description: 'ID of the category task belongs to',
        example: 1,
        type: Number
    }),
    __metadata("design:type", Number)
], CreateTaskDto.prototype, "category", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsArray({}),
    common_1.Optional(),
    api_property_decorator_1.ApiPropertyOptional({
        description: 'labels associated with the task',
        example: [1],
        type: Array(Number)
    }),
    __metadata("design:type", Array)
], CreateTaskDto.prototype, "labels", void 0);
exports.default = CreateTaskDto;
//# sourceMappingURL=create-task.dto.js.map
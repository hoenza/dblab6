import { BaseEntity } from 'typeorm';
import TaskEntity from './task.entity';
export default class SubTaskEntity extends BaseEntity {
    id: number;
    description: string;
    task: TaskEntity;
}

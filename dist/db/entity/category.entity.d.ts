import { BaseEntity } from 'typeorm';
import TaskEntity from './task.entity';
export default class CategoryEntity extends BaseEntity {
    id: number;
    name: string;
    tasks: TaskEntity[];
}

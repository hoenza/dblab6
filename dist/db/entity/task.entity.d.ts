import { BaseEntity } from 'typeorm';
import UserEntity from './user.entity';
import CategoryEntity from './category.entity';
import LabelEntity from './label.entity';
import SubTaskEntity from './subTask.entity';
export default class TaskEntity extends BaseEntity {
    id: number;
    name: string;
    category: CategoryEntity;
    labels: LabelEntity[];
    description: string;
    user: UserEntity;
    subTasks: SubTaskEntity[];
}

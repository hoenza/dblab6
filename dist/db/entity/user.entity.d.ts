import { BaseEntity } from 'typeorm';
import BookEntity from './book.entity';
import TaskEntity from './task.entity';
export default class UserEntity extends BaseEntity {
    id: number;
    pass: string;
    username: string;
    books: BookEntity[];
    tasks: TaskEntity[];
}

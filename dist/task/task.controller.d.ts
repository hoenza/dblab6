import taskEntity from 'src/db/entity/task.entity';
import CreateTaskDto from 'src/todo/dto/create-task.dto';
import { TaskService } from './task.service';
export default class TaskController {
    private readonly taskService;
    constructor(taskService: TaskService);
    insertTask(task: CreateTaskDto): Promise<taskEntity>;
    getAlltasks(): Promise<taskEntity[]>;
    deletetask(taskID: any): Promise<taskEntity>;
    updatetask(taskID: any, task: CreateTaskDto): Promise<taskEntity>;
}

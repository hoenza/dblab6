import TaskEntity from 'src/db/entity/task.entity';
import CreateTaskDto from 'src/todo/dto/create-task.dto';
export declare class TaskService {
    insert(taskDetails: CreateTaskDto): Promise<TaskEntity>;
    getAllTasks(): Promise<TaskEntity[]>;
    delete(TaskID: number): Promise<TaskEntity>;
    update(taskID: number, taskDetails: CreateTaskDto): Promise<TaskEntity>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const dist_1 = require("@nestjs/swagger/dist");
const task_entity_1 = require("../db/entity/task.entity");
const create_task_dto_1 = require("../todo/dto/create-task.dto");
const task_service_1 = require("./task.service");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
let TaskController = class TaskController {
    constructor(taskService) {
        this.taskService = taskService;
    }
    insertTask(task) {
        return this.taskService.insert(task);
    }
    getAlltasks() {
        return this.taskService.getAllTasks();
    }
    deletetask(taskID) {
        return this.taskService.delete(taskID);
    }
    updatetask(taskID, task) {
        return this.taskService.update(taskID, task);
    }
};
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'will handle the creating of new task' }),
    common_1.Post(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    dist_1.ApiBearerAuth(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_task_dto_1.default]),
    __metadata("design:returntype", void 0)
], TaskController.prototype, "insertTask", null);
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'returns the list of all the existing tasks in the database' }),
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    dist_1.ApiBearerAuth(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], TaskController.prototype, "getAlltasks", null);
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'deletes task with taskID id' }),
    common_1.Delete(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    dist_1.ApiBearerAuth(),
    common_1.Delete(),
    __param(0, common_1.Query('taskID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "deletetask", null);
__decorate([
    dist_1.ApiResponse({ status: 200, description: 'will handle the updateing of a task' }),
    common_1.Put(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    dist_1.ApiBearerAuth(),
    __param(0, common_1.Query('taskID')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_task_dto_1.default]),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "updatetask", null);
TaskController = __decorate([
    common_1.Controller('task'),
    __metadata("design:paramtypes", [task_service_1.TaskService])
], TaskController);
exports.default = TaskController;
//# sourceMappingURL=task.controller.js.map
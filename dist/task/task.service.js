"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaskService = void 0;
const category_entity_1 = require("../db/entity/category.entity");
const label_entity_1 = require("../db/entity/label.entity");
const subTask_entity_1 = require("../db/entity/subTask.entity");
const task_entity_1 = require("../db/entity/task.entity");
const user_entity_1 = require("../db/entity/user.entity");
const create_task_dto_1 = require("../todo/dto/create-task.dto");
class TaskService {
    async insert(taskDetails) {
        const { name, description, userID, subTasks, category, labels } = taskDetails;
        const task = new task_entity_1.default();
        task.name = name;
        task.user = await user_entity_1.default.findOne(userID);
        task.labels = [];
        task.category = await category_entity_1.default.findOne(category);
        task.description = description || '';
        for (let i = 0; i < labels.length; i++) {
            const label = await label_entity_1.default.findOne(labels[i]);
            task.labels.push(label);
        }
        await task.save();
        for (let i = 0; i < subTasks.length; i++) {
            const subTask = new subTask_entity_1.default();
            subTask.task = task;
            subTask.description = subTasks[i];
            await subTask.save();
        }
        return task;
    }
    async getAllTasks() {
        return task_entity_1.default.find();
    }
    async delete(TaskID) {
        const Task = await task_entity_1.default.findOne(TaskID);
        return Task.remove();
    }
    async update(taskID, taskDetails) {
        const { name, description, userID, subTasks, category, labels } = taskDetails;
        const task = await task_entity_1.default.findOne(taskID);
        task.name = name;
        task.user = await user_entity_1.default.findOne(userID);
        task.labels = [];
        task.category = await category_entity_1.default.findOne(category);
        task.description = description || '';
        for (let i = 0; i < labels.length; i++) {
            const label = await label_entity_1.default.findOne(labels[i]);
            task.labels.push(label);
        }
        await task.save();
        const prevSubTasks = await subTask_entity_1.default.find({
            where: {
                task: task
            }
        });
        prevSubTasks.forEach(prevSubTask => {
            prevSubTask.remove();
        });
        for (let i = 0; i < subTasks.length; i++) {
            const subTask = new subTask_entity_1.default();
            subTask.task = task;
            subTask.description = subTasks[i];
            await subTask.save();
        }
        return task;
    }
}
exports.TaskService = TaskService;
//# sourceMappingURL=task.service.js.map
import CreateLabelDto from 'src/todo/dto/create-label.dto';
import LabelService from './label.service';
export default class labelController {
    private readonly labelServices;
    constructor(labelServices: LabelService);
    postlabel(label: CreateLabelDto): Promise<import("../db/entity/label.entity").default>;
    getAll(): Promise<import("../db/entity/label.entity").default[]>;
}

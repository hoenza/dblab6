"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const decorators_1 = require("@nestjs/swagger/dist/decorators");
const create_label_dto_1 = require("../todo/dto/create-label.dto");
const label_service_1 = require("./label.service");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
let labelController = class labelController {
    constructor(labelServices) {
        this.labelServices = labelServices;
    }
    postlabel(label) {
        return this.labelServices.insert(label);
    }
    getAll() {
        return this.labelServices.getAlllabel();
    }
};
__decorate([
    decorators_1.ApiResponse({ status: 200, description: 'will handle the creating of new label' }),
    common_1.Post(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    decorators_1.ApiBearerAuth(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_label_dto_1.default]),
    __metadata("design:returntype", void 0)
], labelController.prototype, "postlabel", null);
__decorate([
    decorators_1.ApiResponse({ status: 200, description: 'returns the list of all the existing labels' }),
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    decorators_1.ApiBearerAuth(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], labelController.prototype, "getAll", null);
labelController = __decorate([
    common_1.Controller('label'),
    __metadata("design:paramtypes", [label_service_1.default])
], labelController);
exports.default = labelController;
//# sourceMappingURL=label.controller.js.map
import labelEntity from 'src/db/entity/label.entity';
import CreateLabelDto from 'src/todo/dto/create-label.dto';
export default class LabelService {
    insert(labelDetails: CreateLabelDto): Promise<labelEntity>;
    getAlllabel(): Promise<labelEntity[]>;
}

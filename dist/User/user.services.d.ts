import UserEntity from '../db/entity/user.entity';
import CreateUserDto from './dto/create-user.dto';
import UpdateUserDto from './dto/update-user.dto';
import BookEntity from '../db/entity/book.entity';
export declare class UserServices {
    insert(userDetails: CreateUserDto): Promise<UserEntity>;
    findOne(userName: string): Promise<UserEntity | undefined>;
    getAllUsers(): Promise<UserEntity[]>;
    getBooksOfUser(userID: number): Promise<BookEntity[]>;
    delete(userID: number): Promise<import("typeorm").DeleteResult>;
    put(userDetails: UpdateUserDto): Promise<import("typeorm").UpdateResult>;
}

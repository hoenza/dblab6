import { UserServices } from './user.services';
import CreateUserDto from './dto/create-user.dto';
import UpdateUserDto from './dto/update-user.dto';
export declare class UserController {
    private readonly usersServices;
    constructor(usersServices: UserServices);
    postUser(user: CreateUserDto): Promise<import("../db/entity/user.entity").default>;
    getAll(): Promise<import("../db/entity/user.entity").default[]>;
    getBooks(userID: number): Promise<import("../db/entity/book.entity").default[]>;
    deleteUser(id: number): Promise<import("typeorm").DeleteResult>;
    putUser(user: UpdateUserDto): Promise<import("typeorm").UpdateResult>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const user_services_1 = require("./user.services");
const create_user_dto_1 = require("./dto/create-user.dto");
const swagger_1 = require("@nestjs/swagger");
const update_user_dto_1 = require("./dto/update-user.dto");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
const dist_1 = require("@nestjs/swagger/dist");
let UserController = class UserController {
    constructor(usersServices) {
        this.usersServices = usersServices;
    }
    postUser(user) {
        return this.usersServices.insert(user);
    }
    getAll() {
        return this.usersServices.getAllUsers();
    }
    getBooks(userID) {
        return this.usersServices.getBooksOfUser(userID);
    }
    deleteUser(id) {
        console.log(id);
        return this.usersServices.delete(id);
    }
    putUser(user) {
        return this.usersServices.put(user);
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'will handle the creating of new User' }),
    common_1.Post('singup'),
    dist_1.ApiBody({
        description: 'user name and password',
        schema: {
            type: 'object',
            properties: {
                "username": {
                    type: 'string',
                    example: 'HoEnZa'
                },
                "pass": {
                    type: 'string',
                    example: '8585'
                }
            }
        }
    }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_dto_1.default]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "postUser", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiResponse({ status: 200, description: 'returns the list of all the existing users in the database' }),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], UserController.prototype, "getAll", null);
__decorate([
    common_1.Get('books'),
    swagger_1.ApiResponse({ status: 200, description: 'return all the books which are associated with the userprovided through \'userID\' by the request' }),
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiQuery({ name: 'userID', required: true, type: Number,
        description: `The ID of the user, you are searching for her books.` }),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Body('userID', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "getBooks", null);
__decorate([
    common_1.Delete(),
    swagger_1.ApiResponse({ status: 200, description: 'deletes user which is associated with the userprovided through \'id\' by the request' }),
    swagger_1.ApiQuery({
        name: 'id',
        required: true,
        type: Number,
        description: `The ID of the user, you are to delete.`
    }),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Query('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "deleteUser", null);
__decorate([
    common_1.Put(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiResponse({ status: 200, description: 'will handle the updating of a User' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_user_dto_1.default]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "putUser", null);
UserController = __decorate([
    common_1.Controller('users'),
    __metadata("design:paramtypes", [user_services_1.UserServices])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map
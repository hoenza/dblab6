import { UserServices } from '../User/user.services';
import { JwtService } from '@nestjs/jwt';
export declare class AuthService {
    private userServices;
    private jwtService;
    constructor(userServices: UserServices, jwtService: JwtService);
    validateUser(username: string, pass: string): Promise<any>;
    login(user: any): Promise<{
        access_token: string;
    }>;
}

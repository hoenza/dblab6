import CategoryEntity from 'src/db/entity/category.entity';
import { CreateCategoryDto } from 'src/todo/dto/create-category.dto';
export default class CategoryService {
    insert(categoryDetails: CreateCategoryDto): Promise<CategoryEntity>;
    getAllcategory(): Promise<CategoryEntity[]>;
}

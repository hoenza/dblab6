"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const category_entity_1 = require("../db/entity/category.entity");
const create_category_dto_1 = require("../todo/dto/create-category.dto");
let CategoryService = class CategoryService {
    async insert(categoryDetails) {
        const categoryEntity = category_entity_1.default.create();
        const { name } = categoryDetails;
        categoryEntity.name = name;
        categoryEntity.tasks = [];
        await category_entity_1.default.save(categoryEntity);
        return categoryEntity;
    }
    async getAllcategory() {
        return await category_entity_1.default.find();
    }
};
CategoryService = __decorate([
    common_1.Injectable()
], CategoryService);
exports.default = CategoryService;
//# sourceMappingURL=category.service.js.map
import { CreateCategoryDto } from 'src/todo/dto/create-category.dto';
import CategoryService from './category.service';
export default class CategoryController {
    private readonly categoryServices;
    constructor(categoryServices: CategoryService);
    postcategory(category: CreateCategoryDto): Promise<import("../db/entity/category.entity").default>;
    getAll(): Promise<import("../db/entity/category.entity").default[]>;
}

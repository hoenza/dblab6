"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const decorators_1 = require("@nestjs/swagger/dist/decorators");
const create_category_dto_1 = require("../todo/dto/create-category.dto");
const category_service_1 = require("./category.service");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
let CategoryController = class CategoryController {
    constructor(categoryServices) {
        this.categoryServices = categoryServices;
    }
    postcategory(category) {
        return this.categoryServices.insert(category);
    }
    getAll() {
        return this.categoryServices.getAllcategory();
    }
};
__decorate([
    decorators_1.ApiResponse({ status: 200, description: 'will handle the creating of new category' }),
    common_1.Post(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    decorators_1.ApiBearerAuth(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_category_dto_1.CreateCategoryDto]),
    __metadata("design:returntype", void 0)
], CategoryController.prototype, "postcategory", null);
__decorate([
    decorators_1.ApiResponse({ status: 200, description: 'returns the list of all the existing categories in the database' }),
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    decorators_1.ApiBearerAuth(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CategoryController.prototype, "getAll", null);
CategoryController = __decorate([
    common_1.Controller('category'),
    __metadata("design:paramtypes", [category_service_1.default])
], CategoryController);
exports.default = CategoryController;
//# sourceMappingURL=category.controller.js.map
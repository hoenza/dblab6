"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const books_service_1 = require("./books.service");
const create_book_dto_1 = require("./dto/create-book.dto");
const update_book_dto_1 = require("./dto/update-book.dto");
const swagger_1 = require("@nestjs/swagger");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
let GenreController = class GenreController {
    constructor(booksService) {
        this.booksService = booksService;
    }
    postGenre(book) {
        return this.booksService.insert(book);
    }
    getAll() {
        return this.booksService.getAllBooks();
    }
    deleteUser(id) {
        return this.booksService.delete(id);
    }
    putUser(book) {
        return this.booksService.put(book);
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'will handle the creating of new Book' }),
    common_1.Post('post'),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_book_dto_1.default]),
    __metadata("design:returntype", void 0)
], GenreController.prototype, "postGenre", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'returns the list of all the existing books in the database' }),
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], GenreController.prototype, "getAll", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'deletes book with bookID id' }),
    common_1.Delete(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    __param(0, common_1.Query('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], GenreController.prototype, "deleteUser", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'will handle the updateing of a Book' }),
    common_1.Put(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    swagger_1.ApiBearerAuth(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_book_dto_1.default]),
    __metadata("design:returntype", void 0)
], GenreController.prototype, "putUser", null);
GenreController = __decorate([
    common_1.Controller('book'),
    __metadata("design:paramtypes", [books_service_1.BooksService])
], GenreController);
exports.default = GenreController;
//# sourceMappingURL=books.controller.js.map
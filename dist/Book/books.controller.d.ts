import { BooksService } from './books.service';
import CreateBookDto from './dto/create-book.dto';
import UpdateBookDto from './dto/update-book.dto';
export default class GenreController {
    private readonly booksService;
    constructor(booksService: BooksService);
    postGenre(book: CreateBookDto): Promise<import("../db/entity/book.entity").default>;
    getAll(): Promise<import("../db/entity/book.entity").default[]>;
    deleteUser(id: number): Promise<import("typeorm").DeleteResult>;
    putUser(book: UpdateBookDto): Promise<import("typeorm").UpdateResult>;
}

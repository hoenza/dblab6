import BookEntity from '../db/entity/book.entity';
import CreateBookDto from './dto/create-book.dto';
import UpdateBookDto from './dto/update-book.dto';
export declare class BooksService {
    insert(bookDetails: CreateBookDto): Promise<BookEntity>;
    getAllBooks(): Promise<BookEntity[]>;
    delete(bookID: number): Promise<import("typeorm").DeleteResult>;
    put(bookDetails: UpdateBookDto): Promise<import("typeorm").UpdateResult>;
}

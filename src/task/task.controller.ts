import { Body, Controller, Delete, Get, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiBearerAuth } from '@nestjs/swagger/dist';
import taskEntity from 'src/db/entity/task.entity';
import CreateTaskDto from 'src/todo/dto/create-task.dto';
import { TaskService } from './task.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';


@Controller('task')
export default class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @ApiResponse({ status: 200, description: 'will handle the creating of new task'})
  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  insertTask(@Body() task: CreateTaskDto) {
    return this.taskService.insert(task);
  }

  @ApiResponse({ status: 200, description: 'returns the list of all the existing tasks in the database'})
  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  getAlltasks() {
    return this.taskService.getAllTasks();
  }


  @ApiResponse({ status: 200, description: 'deletes task with taskID id'})
  @Delete()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Delete()
  deletetask(@Query('taskID') taskID): Promise<taskEntity> {
    return this.taskService.delete(taskID);
  }

  @ApiResponse({ status: 200, description: 'will handle the updateing of a task'})
  @Put()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  updatetask(@Query('taskID') taskID, @Body() task: CreateTaskDto): Promise<taskEntity> {
    return this.taskService.update(taskID, task);
  }
}
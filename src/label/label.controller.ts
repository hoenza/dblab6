import { Body, Controller, Get, Post, UseGuards  } from '@nestjs/common';
import { ApiResponse, ApiBearerAuth, } from '@nestjs/swagger/dist/decorators';
import CreateLabelDto from 'src/todo/dto/create-label.dto';
import LabelService from './label.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('label')
export default class labelController {
  constructor(private readonly labelServices: LabelService) {}
  
  @ApiResponse({ status: 200, description: 'will handle the creating of new label'})
  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  postlabel( @Body() label: CreateLabelDto) {
    return this.labelServices.insert(label);
  }

  @ApiResponse({ status: 200, description: 'returns the list of all the existing labels'})
  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  getAll() {
    return this.labelServices.getAlllabel();
  }
}
import BookEntity from '../db/entity/book.entity';
import CreateBookDto from './dto/create-book.dto';
import UpdateBookDto from './dto/update-book.dto';
import UserEntity from '../db/entity/user.entity';
import { createQueryBuilder, getConnection } from 'typeorm';
import GenreEntity from '../db/entity/genre.entity';

export class BooksService {

    async insert(bookDetails: CreateBookDto): Promise<BookEntity> {
        const { name , userID , genreIDs } = bookDetails;
        const book = new BookEntity();
        book.name = name;
        book.user = await UserEntity.findOne(userID) ;
        book.genres=[];
        for ( let i = 0; i < genreIDs.length ; i++)
        {
                    const genre = await GenreEntity.findOne(genreIDs[i]);
                    book.genres.push(genre);
        }
        await book.save();
        return book;
    }

    async getAllBooks(): Promise<BookEntity[] > {
        // const user: UserEntity = await UserEntity.findOne({where: {id: 2}, relations: ['books']});
        return BookEntity.find();
    }

    async delete(bookID: number) {
        console.log(typeof(bookID));
        return await BookEntity.delete({id: bookID});
    }
    
    async put(bookDetails: UpdateBookDto) {
        await BookEntity.update(bookDetails.id, {user: await UserEntity.findOne(bookDetails.userID)});
        return await BookEntity.update(bookDetails.id, {name: bookDetails.name});
    }
}
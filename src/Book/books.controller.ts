import { Body, Controller, Delete, Get, ParseIntPipe, Post, Put, Query, UseGuards } from '@nestjs/common';
import { BooksService } from './books.service';
import CreateBookDto from './dto/create-book.dto';
import UpdateBookDto from './dto/update-book.dto';
import { ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';



@Controller('book')
export default class GenreController {
    constructor(private readonly booksService: BooksService) {}

    @ApiResponse({ status: 200, description: 'will handle the creating of new Book'})
    @Post('post')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    postGenre( @Body() book: CreateBookDto) {
    return this.booksService.insert(book);
    }

    @ApiResponse({ status: 200, description: 'returns the list of all the existing books in the database'})
    @Get()
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    getAll() {
    return this.booksService.getAllBooks();
    }

    @ApiResponse({ status: 200, description: 'deletes book with bookID id'})
    @Delete()
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    deleteUser( @Query('id', ParseIntPipe) id: number) {
        return this.booksService.delete(id);
    }

    @ApiResponse({ status: 200, description: 'will handle the updateing of a Book'})
    @Put()
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    putUser( @Body() book: UpdateBookDto) {
        return this.booksService.put(book);
    }
}
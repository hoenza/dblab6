import { Controller, Get, Request, Post, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { AuthService } from './auth/auth.service';
import { ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import { ApiBody } from '@nestjs/swagger/dist';


@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  @ApiResponse({
    description: 'use this api to login to app', 
    status: 201
  })
  @ApiBody({
    description: 'user name and password',
    schema: {
      type: 'object',
      properties: {
        "username": {
          type: 'string', 
          example: 'HoEnZa'
        }, 
        "password": {
          type: 'string', 
          example: '8585'
        }
      }
    }
  })
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import GenreServices from './genre.services';
import CreateGenreDto from './dto/create-genre.dto';
import { ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';


@Controller('genre')
export default class GenreController {
    constructor(private readonly genreServices: GenreServices) {}

    @ApiResponse({ status: 200, description: 'will handle the creating of new Genre'})
    @Post('post')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    postGenre( @Body() genre: CreateGenreDto) {
    return this.genreServices.insert(genre);
    }

    @ApiResponse({ status: 200, description: 'returns the list of all the existing Genres in the database'})
    @Get()
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    getAll() {
    return this.genreServices.getAllGenre();
    }
}
import { Optional } from "@nestjs/common";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger/dist/decorators/api-property.decorator";
import { IsArray, isNumber, IsNumber, IsOptional, Length } from "class-validator";

export default class CreateTaskDto {

    readonly name: string;

    readonly description: string;

    readonly userID: number;

    readonly subTasks: string[];

    readonly category: number;

    readonly labels: number[];
    
  }
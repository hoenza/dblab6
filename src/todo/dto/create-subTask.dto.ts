import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger/dist/decorators/api-property.decorator";
import { IsNumber } from "class-validator";

export default class CreateSubTaskDto {

    readonly description: string;

    readonly taskID: number;

  }
import { Body, Controller, Get, Post,  UseGuards  } from '@nestjs/common';
import { ApiResponse, ApiBearerAuth, } from '@nestjs/swagger/dist/decorators';
import { CreateCategoryDto } from 'src/todo/dto/create-category.dto';
import CategoryService from './category.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';


@Controller('category')
export default class CategoryController {
  constructor(private readonly categoryServices: CategoryService) {}
  
  @ApiResponse({ status: 200, description: 'will handle the creating of new category'})
  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  postcategory( @Body() category: CreateCategoryDto) {
    return this.categoryServices.insert(category);
  }

  @ApiResponse({ status: 200, description: 'returns the list of all the existing categories in the database'})
  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  getAll() {
    return this.categoryServices.getAllcategory();
  }
}
import { Body, Controller, Delete, Get, ParseIntPipe, Post, Put, Query, UseGuards } from '@nestjs/common';
import { UserServices } from './user.services';
import CreateUserDto from './dto/create-user.dto';
import { ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import UpdateUserDto from './dto/update-user.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody } from '@nestjs/swagger/dist';
import { LocalAuthGuard } from '../auth/local-auth.guard';


@Controller('users')
export class UserController {
    constructor(private readonly usersServices: UserServices) {}

    @ApiResponse({ status: 200, description: 'will handle the creating of new User'})
    @Post('singup')
    @ApiBody({
        description: 'user name and password',
        schema: {
            type: 'object',
            properties: {
            "username": {
                type: 'string', 
                example: 'HoEnZa'
            }, 
            "pass": {
                type: 'string', 
                example: '8585'
            }
            }
        }
    })
    postUser( @Body() user: CreateUserDto) {
        return this.usersServices.insert(user);
    }

    @Get()
    @ApiResponse({ status: 200, description: 'returns the list of all the existing users in the database'})
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    getAll() {
        return this.usersServices.getAllUsers();
    }
    
    @Get('books')
    @ApiResponse({ status: 200, description: 'return all the books which are associated with the userprovided through \'userID\' by the request'})
    @ApiBearerAuth()
    @ApiQuery({name: 'userID', required: true, type: Number,
        description :`The ID of the user, you are searching for her books.`})
    @UseGuards(JwtAuthGuard)
    getBooks( @Body('userID', ParseIntPipe) userID: number ) {
        return this.usersServices.getBooksOfUser(userID);
    }


    @Delete()
    @ApiResponse({ status: 200, description: 'deletes user which is associated with the userprovided through \'id\' by the request'})
    @ApiQuery({
    name: 'id',
    required: true,
    type: Number,
    description :`The ID of the user, you are to delete.`
    })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    deleteUser( @Query('id', ParseIntPipe) id: number) {
        console.log(id);
        return this.usersServices.delete(id);
    }


    @Put()
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiResponse({ status: 200, description: 'will handle the updating of a User'})
    putUser( @Body() user: UpdateUserDto) {
        return this.usersServices.put(user);
    }

}
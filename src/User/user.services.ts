import { Injectable } from '@nestjs/common';
import UserEntity from '../db/entity/user.entity';
import CreateUserDto from './dto/create-user.dto';
import UpdateUserDto from './dto/update-user.dto'
import BookEntity from '../db/entity/book.entity';
import {getConnection} from "typeorm";

@Injectable()
export class UserServices {

  async insert(userDetails: CreateUserDto): Promise<UserEntity> {
    const userEntity: UserEntity = UserEntity.create();
    const {username , pass } = userDetails;
    userEntity.username = username;
    userEntity.pass = pass;
    await UserEntity.save(userEntity);
    return userEntity;
  }

  async findOne(userName: string): Promise<UserEntity | undefined> {
    return await UserEntity.findOne({where: {username: userName}});
  }

  async getAllUsers(): Promise<UserEntity[]> {
    return await UserEntity.find();
  }

  async getBooksOfUser(userID: number): Promise<BookEntity[]> {
    console.log(typeof(userID));
    const user: UserEntity = await UserEntity.findOne({where: {id: userID}, relations: ['books']});
    return user.books;
  }

  async delete(userID: number) {
    console.log(typeof(userID));
    return await UserEntity.delete({id: userID});
  }

  async put(userDetails: UpdateUserDto) {
    return await UserEntity.update(userDetails.id, {username: userDetails.name});
  }
}
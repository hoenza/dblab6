export default class UpdateUserDto {
    readonly id: number;
    readonly name: string;
    readonly pass: string;
    readonly books: number[];
}
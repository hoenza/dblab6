import { Controller, Post, Body, Get, Delete, Query, Put, UseGuards } from "@nestjs/common";
import { ApiResponse, ApiQuery } from "@nestjs/swagger/dist";
import SubTaskEntity from "src/db/entity/subTask.entity";
import taskEntity from "src/db/entity/task.entity";
import CreateSubTaskDto from "src/todo/dto/create-subTask.dto";
import { SubTaskService } from "./sub-task.service";
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';


@Controller('subtask')
export default class SubTaskController {
  constructor(private readonly subTaskService: SubTaskService) {}

  @ApiResponse({ status: 200, description: 'will handle the creating of new sub-task'})
  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  insertTask(@Body() task: CreateSubTaskDto) {
    return this.subTaskService.insert(task);
  }

  @ApiResponse({ status: 200, description: 'returns the list of all the existing sub-tasks in the database'})
  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  getAlltasks() {
    return this.subTaskService.getAllSubTasks();
  }


  @ApiResponse({ status: 200, description: 'deletes subtask with id'})
  @Delete()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Delete()
  deletetask(@Query('subTaskID') subTaskID): Promise<SubTaskEntity> {
    return this.subTaskService.delete(subTaskID);
  }

  @ApiResponse({ status: 200, description: 'will handle the updateing of a sub-task'})
  @Put()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Put()
  updatetask(@Query('subTaskID') subTaskID, @Body() subTask: CreateSubTaskDto): Promise<SubTaskEntity> {
    return this.subTaskService.update(subTaskID, subTask);
  }
}
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Length, IsOptional, Min, IsNumber } from 'class-validator';

export class PersonDto {

    name: string;

    year: number;
    
}
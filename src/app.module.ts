import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HelloModule } from './hello/hello.module';

import { UserModule } from './User/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import UserEntity from './db/entity/user.entity';
import BooksModule from './Book/books.module';
import GenreModule from './Genre/genre.module';
import BookEntity from './db/entity/book.entity';
import GenreEntity from './db/entity/genre.entity';
import { TaskService } from './task/task.service';
import { TaskModule } from './task/task.module';
import { CategoryModule } from './category/category.module';
import { LabelModule } from './label/label.module';
import { SubTaskModule } from './sub-task/sub-task.module';

@Module({
  imports: [HelloModule,
            UserModule ,
            BooksModule,
            GenreModule,
            TypeOrmModule.forFeature(
              [UserEntity, BookEntity , GenreEntity],
            ),
        
            TypeOrmModule.forRoot(),
        
            AuthModule,
            TaskModule,
            CategoryModule,
            LabelModule,
            SubTaskModule,
          ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, PrimaryColumn } from 'typeorm';
import BookEntity from './book.entity';
import TaskEntity from './task.entity';

@Entity()
export default class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    pass: string;

    @Column({ length: 500 })
    username: string;

    @OneToMany( type => BookEntity , book => book.user)
    books: BookEntity[];   
    
    @OneToMany(type => TaskEntity, task => task.user)
    tasks: TaskEntity[]; 
}